from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request:
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state:
    url = "https://api.pexels.com/v1/search"
    parameters = {
        "query": f"{city} {state} US",
        "per_page": 1,
    }
    # Make the request:
    response = requests.get(url, params=parameters, headers=headers)
    try:
        # Parse the JSON response,
        # Return a dictionary that contains a `picture_url` key and
        #   one of the URLs for one of the pictures in the response:
        photo_location_url = response.json()["photos"][0]["src"]["original"]
        return photo_location_url
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state:
    url = "http://api.openweathermap.org/geo/1.0/direct"
    parameters = {"q": f"{city},{state},US", "appid": OPEN_WEATHER_API_KEY}
    # Make the request:
    response = requests.get(url, params=parameters)
    # Parse the JSON response,
    # Get the latitude and longitude from the response:
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]
    # Create the URL for the current weather API with the latitude
    #   and longitude:
    url = "https://api.openweathermap.org/data/2.5/weather"
    parameters = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # Make the request:
    response = requests.get(url, params=parameters)
    try:
        # Parse the JSON response,
        # Get the main temperature and the weather's description and put
        # them in a dictionary:
        temperature = response.json()["main"]["temp"]
        description = response.json()["weather"][0]["description"]
        # Return the dictionary
        return {"temperature": temperature, "description": description}
    except (KeyError, IndexError):
        return None
